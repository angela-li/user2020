# Tutorial Sessions

## Morning

- **First steps in spatial data handling and visualization** &mdash; [S. Rochette](https://github.com/statnmap), [D. Scott](https://dscott.netlify.com/), and [J. Nowosad](https://nowosad.github.io/)
- **Predictive modeling with text using tidy data principles** &mdash; [J. Silge](https://juliasilge.com/) and E. Hansen
- **So, you want to learn Python? An introduction to Python for the R lover** &mdash; [S. Ellis](http://www.shanellis.com/)
- **Application of Gaussian graphical models to metabolomics** &mdash; [D. Scholtens](https://www.feinberg.northwestern.edu/faculty-profiles/az/profile.html?xid=16163) and [R. Balasubramanian](https://www.umass.edu/sphhs/person/faculty/raji-balasubramanian)
- **Periscope and CanvasXpress – Creating an enterprise-grade big-data visualization
application in a day** &mdash; [C. Brett](https://github.com/cb4ds)
- **Seamless R and C++ integration with Rcpp** &mdash; [D. Eddelbuettel](http://dirk.eddelbuettel.com/)
- **Create and share reproducible code with R Markdown and workflowr** &mdash; [J. Blischak](https://jdblischak.com/)
- **Causal inference in R** &mdash; [L. D'Agostino McGowan](https://www.lucymcgowan.com/) and [M. Barrett](https://malco.io/)
- **Reproducible computation at scale with drake: hands-on practice with a machine
learning project** &mdash; [W. Landau](https://wlandau.github.io/) 

## Afternoon

- **How green was my valley - Spatial analytics with PostgreSQL, PostGIS, R, and PL/R** &mdash; [J. Conway](https://joeconway.com/)
- **Creating beautiful data visualizations in R: a ggplot2 crash course** &mdash; [S. Tyner](https://sctyner.github.io/)
- **Building interactive web applications with Dash for R** &mdash; [R. Kyle](https://github.com/rpkyle)
- **Easy Larger-than-RAM data manipulation with disk.frame** &mdash; [ZJ Dai](https://github.com/xiaodaigh)
5.R Markdown recipes &mdash; [Y. Xie](https://yihui.org/en/)
- **Getting the most out of Git** &mdash; [C. Gillespie](https://github.com/csgillespie) and [R. Davies](https://trianglegirl.rbind.io/)
- **End-to-end machine learning with Metaflow: Going from prototype to production
with Netflix’s open source project for reproducible data science** &mdash; [S. Goyal](https://github.com/savingoyal) and B.
Galvin
- **Package development** &mdash; [J. Hester](https://github.com/jimhester) and [H. Wickham](http://hadley.nz/)
- **What they forgot to teach you about teaching R** &mdash; [M. Cetinkaya-Rundel](http://www2.stat.duke.edu/~mc301/)
